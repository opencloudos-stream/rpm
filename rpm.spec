Summary:       The RPM Package Manager (RPM)
Name:          rpm
Version:       4.18.2
Release:       4%{?dist}
License:       GPLv2+
Url:           http://www.rpm.org/
%define major_ver %(echo %version | cut -d. -f1)
%define minor_ver %(echo %version | cut -d. -f2)
Source0:       http://ftp.rpm.org/releases/rpm-%{major_ver}.%{minor_ver}.x/rpm-%{version}.tar.bz2
Source1:       rpmdb-rebuild.service
Source2:       rpmdb-migrate.service
Source3:       rpmdb_migrate

Patch3000:     rpm-4.17.x-rpm_dbpath.patch
Patch3001:     rpm-4.18.x-siteconfig.patch
Patch3002:     rpm-4.9.90-no-man-dirs.patch
Patch3003:     rpm-4.18.x-ldflags.patch
Patch3004:     rpm-4.18.x-revert-pandoc-cond.patch


BuildRequires: systemd-rpm-macros gcc make gawk
BuildRequires: elfutils-devel elfutils-libelf-devel readline-devel zlib-devel
BuildRequires: openssl-devel popt-devel file-devel gettext-devel ncurses-devel
BuildRequires: bzip2-devel lua-devel libcap-devel libacl-devel xz-devel
BuildRequires: libarchive-devel libzstd-devel sqlite-devel
BuildRequires: libselinux-devel dbus-devel audit-libs-devel ima-evm-utils-devel fsverity-utils-devel
BuildRequires: gnupg2 debugedit automake libtool
Requires: coreutils popt curl

%description
The RPM Package Manager (RPM) is a powerful package management system capable of

- building computer software from source into easily distributable packages
- installing, updating and uninstalling packaged software
- querying detailed information about the packaged software, whether installed or not
- verifying integrity of packaged software and resulting software installation

%package libs
Summary:  Libraries for manipulating RPM packages
License: GPLv2+ and LGPLv2+ with exceptions
Requires: %{name} = %{version}-%{release}

%description libs
This package contains the RPM shared libraries.

%package build-libs
Summary:  Libraries for building RPM packages
License: GPLv2+ and LGPLv2+ with exceptions
Requires: rpm-libs = %{version}-%{release}

%description build-libs
This package contains the RPM shared libraries for building packages.

%package sign-libs
Summary:  Libraries for signing RPM packages
License: GPLv2+ and LGPLv2+ with exceptions
Requires: rpm-libs = %{version}-%{release}
Requires: %{_bindir}/gpg2

%description sign-libs
This package contains the RPM shared libraries for signing packages.

%package devel
Summary:  Development files for manipulating RPM packages
License: GPLv2+ and LGPLv2+ with exceptions
Requires: %{name} = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}
Requires: %{name}-build-libs = %{version}-%{release}
Requires: %{name}-sign-libs = %{version}-%{release}
Requires: popt-devel

%description devel
RPM C library and header files.

%package build
Summary: Scripts and executable programs used to build packages
Requires: rpm = %{version}-%{release}
Requires: elfutils binutils
Requires: findutils sed grep gawk diffutils file patch
Requires: tar unzip gzip bzip2 cpio xz
Requires: zstd debugedit pkgconfig system-rpm-config
Requires: /usr/bin/gdb-add-index
Suggests: gdb-minimal

%description build
The rpm-build package contains the scripts and executable programs
that are used to build packages using the RPM Package Manager.

%package sign
Summary: Package signing support
Requires: rpm-sign-libs = %{version}-%{release}

%description sign
This package contains support for digitally signing RPM packages.

%package -n python3-%{name}
Summary: Python 3 bindings for apps which will manipulate RPM packages
BuildRequires: python3-devel
Requires: %{name}-libs = %{version}-%{release}
Provides: %{name}-python3 = %{version}-%{release}
Provides: python3-%{name}

%description -n python3-%{name}
Python 3 module for RPM

%package cron
Summary: Create daily logs of installed packages.
BuildArch: noarch
Requires: crontabs logrotate rpm = %{version}-%{release}

%description cron
This package contains a cron job which creates daily logs of installed
packages on a system.

%package plugin-selinux
Summary: Rpm plugin for SELinux functionality
Requires: rpm-libs = %{version}-%{release}
Requires: selinux-policy-base

%description plugin-selinux
%{summary}.

%package plugin-syslog
Summary: Rpm plugin for syslog functionality
Requires: rpm-libs = %{version}-%{release}

%description plugin-syslog
%{summary}.

%package plugin-systemd-inhibit
Summary: Rpm plugin for systemd inhibit functionality
Requires: rpm-libs = %{version}-%{release}

%description plugin-systemd-inhibit
This plugin blocks systemd from entering idle, sleep or shutdown while an rpm
transaction is running using the systemd-inhibit mechanism.

%package plugin-ima
Summary: Rpm plugin ima file signatures
Requires: rpm-libs = %{version}-%{release}

%description plugin-ima
%{summary}.

%package plugin-prioreset
Summary: Rpm plugin for resetting scriptlet priorities for SysV init
Requires: rpm-libs = %{version}-%{release}

%description plugin-prioreset
%{summary}.

Useful on legacy SysV init systems if you run rpm transactions with
nice/ionice priorities. Should not be used on systemd systems.

%package plugin-audit
Summary: Rpm plugin for logging audit events on package operations
Requires: rpm-libs = %{version}-%{release}

%description plugin-audit
%{summary}.

%package plugin-fsverity
Summary: Rpm plugin for fsverity file signatures
Requires: rpm-libs = %{version}-%{release}

%description plugin-fsverity
%{summary}.

%package plugin-fapolicyd
Summary: Rpm plugin for fapolicyd support
Requires: rpm-libs = %{version}-%{release}
Provides: fapolicyd-plugin = %{version}-%{release}

%description plugin-fapolicyd
%{summary}.

See https://github.com/linux-application-whitelisting/fapolicyd for information about
the fapolicyd daemon.

%package plugin-dbus-announce
Summary: Rpm plugin for announcing transactions on the DBUS
Requires: rpm-libs = %{version}-%{release}

%description plugin-dbus-announce
Send rpm transaction info to system DBUS, notify other programs 
of package removal or installation.

%prep
%autosetup -n rpm-%{version} -p1

# adapt rpm man to current OS
rpmrc_path="%{_rpmconfigdir}/%{_vendor}"
sed -i "s?\/usr\/lib\/rpm\/redhat?$rpmrc_path?g" docs/man/rpm.8*
# fix the probabilistic failure issue due to the modification time of the man file.
touch -r docs/man/rpm.8 docs/man/rpm.8.md

%build
autoreconf -fi

sed -i -e 's|compiler_flags=$|compiler_flags="%{_hardened_ldflags}"|' build-aux/ltmain.sh

./configure \
    --prefix=%{_usr} \
    --sysconfdir=%{_sysconfdir} \
    --localstatedir=%{_var} \
    --sharedstatedir=%{_var}/lib \
    --libdir=%{_libdir} \
    --build=%{_target_platform} \
    --host=%{_target_platform} \
    --with-vendor=%{_vendor} \
    --with-lua \
    --with-selinux \
    --with-cap \
    --with-acl \
    --with-fapolicyd \
    --with-crypto=openssl \
    --with-imaevm \
    --with-fsverity \
    --enable-ndb \
    --enable-zstd \
    --enable-sqlite \
    --enable-bdb-ro \
    --enable-python

%make_build

pushd python
  %py3_build
popd

%install
%make_install

pushd python
  %py3_install
popd

mkdir -p %{buildroot}%{_unitdir}
install -m 644 %{SOURCE1} %{buildroot}%{_unitdir}
install -m 644 %{SOURCE2} %{buildroot}%{_unitdir}

mkdir -p %{buildroot}%{rpmhome}
install -m 755 %{SOURCE3} %{buildroot}%{rpmhome}

install -D -m 755 scripts/rpm.daily %{buildroot}%{_sysconfdir}/cron.daily/rpm/rpm.daily
install -D -m 644 scripts/rpm.log %{buildroot}%{_sysconfdir}/logrotate.d/rpm/rpm.log

mkdir -p %{buildroot}%{_sysconfdir}/rpm
mkdir -p %{buildroot}/usr/lib/rpm/macros.d
mkdir -p %{buildroot}/usr/lib/sysimage/rpm

for be in ndb sqlite; do
    ./rpmdb --define "_db_backend ${be}" --dbpath=${PWD}/${be} --initdb
    cp -va ${be}/. %{buildroot}/usr/lib/sysimage/rpm/
done

ln -s ../../bin/find-debuginfo %{buildroot}/usr/lib/rpm/find-debuginfo.sh

find %{buildroot} -name "*.la" -delete

rm -f %{buildroot}/usr/lib/rpm/{perldeps.pl,perl.*,pythond*}
rm -f %{buildroot}%{_fileattrsdir}/{perl*,python*}
rm -rf %{buildroot}/var/tmp

%find_lang rpm

%check
make check TESTSUITEFLAGS=-j%{_smp_build_ncpus} || (cat tests/rpmtests.log; exit 1)
make clean


%posttrans
if [ ! -d /var/lib/rpm ] && [ -d /usr/lib/sysimage/rpm ] && [ ! -f /usr/lib/sysimage/rpm/.rpmdbdirsymlink_created ]; then
    ln -sfr /usr/lib/sysimage/rpm /var/lib/rpm
    touch /usr/lib/sysimage/rpm/.rpmdbdirsymlink_created
fi

%files -f rpm.lang
%license COPYING
%doc CREDITS docs/manual/[a-z]*
%{_unitdir}/rpmdb-rebuild.service
%{_unitdir}/rpmdb-migrate.service
%dir %{_sysconfdir}/rpm
%attr(0755, root, root) %dir /usr/lib/sysimage/rpm
%attr(0644, root, root) %ghost %config(missingok,noreplace) /usr/lib/sysimage/rpm/*
%attr(0644, root, root) %ghost /usr/lib/sysimage/rpm/.*.lock
%{_bindir}/rpm
%{_bindir}/rpm2archive
%{_bindir}/rpm2cpio
%{_bindir}/rpmdb
%{_bindir}/rpmkeys
%{_bindir}/rpmquery
%{_bindir}/rpmverify
%{_mandir}/man8/rpm.8*
%{_mandir}/man8/rpmdb.8*
%{_mandir}/man8/rpmkeys.8*
%{_mandir}/man8/rpm2archive.8*
%{_mandir}/man8/rpm2cpio.8*
%{_mandir}/man8/rpm-misc.8*
%{_mandir}/man8/rpm-plugins.8*
%lang(fr) %{_mandir}/fr/man[18]/*.[18]*
%lang(ko) %{_mandir}/ko/man[18]/*.[18]*
%lang(ja) %{_mandir}/ja/man[18]/*.[18]*
%lang(pl) %{_mandir}/pl/man[18]/*.[18]*
%lang(ru) %{_mandir}/ru/man[18]/*.[18]*
%lang(sk) %{_mandir}/sk/man[18]/*.[18]*
%attr(0755, root, root) %dir /usr/lib/rpm
/usr/lib/rpm/macros
/usr/lib/rpm/macros.d
/usr/lib/rpm/lua
/usr/lib/rpm/rpmpopt*
/usr/lib/rpm/rpmrc
/usr/lib/rpm/rpmdb_*
/usr/lib/rpm/rpm.daily
/usr/lib/rpm/rpm.log
/usr/lib/rpm/rpm.supp
/usr/lib/rpm/rpm2cpio.sh
/usr/lib/rpm/tgpg
/usr/lib/rpm/platform
%dir /usr/lib/rpm/fileattrs

%files libs
%{_libdir}/librpmio.so.9
%{_libdir}/librpm.so.9
%{_libdir}/librpmio.so.9.*
%{_libdir}/librpm.so.9.*
%dir %{_libdir}/rpm-plugins

%files plugin-syslog
%{_libdir}/rpm-plugins/syslog.so
%{_mandir}/man8/rpm-plugin-syslog.8*

%files plugin-selinux
%{_libdir}/rpm-plugins/selinux.so
%{_mandir}/man8/rpm-plugin-selinux.8*

%files plugin-systemd-inhibit
%{_libdir}/rpm-plugins/systemd_inhibit.so
%{_mandir}/man8/rpm-plugin-systemd-inhibit.8*

%files plugin-ima
%{_libdir}/rpm-plugins/ima.so
%{_mandir}/man8/rpm-plugin-ima.8*

%files plugin-fsverity
%{_libdir}/rpm-plugins/fsverity.so

%files plugin-fapolicyd
%{_libdir}/rpm-plugins/fapolicyd.so
%{_mandir}/man8/rpm-plugin-fapolicyd.8*

%files plugin-prioreset
%{_libdir}/rpm-plugins/prioreset.so
%{_mandir}/man8/rpm-plugin-prioreset.8*

%files plugin-audit
%{_libdir}/rpm-plugins/audit.so
%{_mandir}/man8/rpm-plugin-audit.8*

%files plugin-dbus-announce
%{_libdir}/rpm-plugins/dbus_announce.so
%{_mandir}/man8/rpm-plugin-dbus-announce.8*
%{_sysconfdir}/dbus-1/system.d/org.rpm.conf

%files build-libs
%{_libdir}/librpmbuild.so.9
%{_libdir}/librpmbuild.so.9.*

%files sign-libs
%{_libdir}/librpmsign.so.9
%{_libdir}/librpmsign.so.9.*

%files build
%{_bindir}/rpmbuild
%{_bindir}/gendiff
%{_bindir}/rpmspec
%{_bindir}/rpmlua
%{_mandir}/man1/gendiff.1*
%{_mandir}/man8/rpmbuild.8*
%{_mandir}/man8/rpmdeps.8*
%{_mandir}/man8/rpmspec.8*
%{_mandir}/man8/rpmlua.8*
/usr/lib/rpm/brp-*
/usr/lib/rpm/check-*
/usr/lib/rpm/find-lang.sh
/usr/lib/rpm/*provides*
/usr/lib/rpm/*requires*
/usr/lib/rpm/*deps*
/usr/lib/rpm/*.prov
/usr/lib/rpm/*.req
/usr/lib/rpm/mkinstalldirs
/usr/lib/rpm/fileattrs/*
/usr/lib/rpm/find-debuginfo.sh
/usr/lib/rpm/rpmuncompress

%files sign
%{_bindir}/rpmsign
%{_mandir}/man8/rpmsign.8*

%files -n python3-%{name}
%{python3_sitearch}/rpm/
%{python3_sitearch}/rpm-%{version}*.egg-info

%files devel
%{_mandir}/man8/rpmgraph.8*
%{_bindir}/rpmgraph
%{_libdir}/librp*[a-z].so
%{_libdir}/pkgconfig/rpm.pc
%{_includedir}/rpm/

%files cron
%{_sysconfdir}/cron.daily/rpm
%config(noreplace) %{_sysconfdir}/logrotate.d/rpm

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.18.2-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.18.2-3
- Rebuilt for loongarch release

* Wed Jul 10 2024 cunshunxia <cunshunxia@tencent.com> - 4.18.2-2
- fix the probabilistic failure issue due to the modification time of the man file.

* Wed Jan 17 2024 Upgrade Robot <upbot@opencloudos.org> - 4.18.2-1
- Upgrade to version 4.18.2

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.18.1-4
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.18.1-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Aug 04 2023 Xiaojie Chen <jackxjchen@tencent.com> - 4.18.1-2
- Rebuilt for file 5.45

* Wed Aug 02 2023 rockerzhu <rockerzhu@tencent.com> - 4.18.1-1
- Update to 4.18.1

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.17.1-4
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.17.1-3
- Rebuilt for OpenCloudOS Stream 23

* Wed Oct 26 2022 Shuo Wang <abushwang@tencent.com> - 4.17.1-2
- adapt rpm man to current OS

* Fri Jul 15 2022 Xun Zhong <invain@opencloudos.tech> - 4.17.1-1
- initial build
